# temp

Small CLI utility for quickly creating a project folder with

Will open `$EDITOR` by default. You can specify a custom hook in the config directory.

# Installation
When you have cargo installed, you can run:
```bash
cargo install --git https://gitlab.com/DrRuhe/temp.git
```
To use this program with nix just use the packages exposed by the flake.



# Usage

Empty Project:
```bash
~$ temp
```

Project with files of current directory:
```bash
~$ temp *
```

Project symlinks to current files:
```bash
~$ temp -s *
```

Pass additional args to the project create hook:
```bash
~$ temp * -- arg1 arg2
```

# Setting up the Hook
temp will try to execute a hook when creating a project. By default this file will be `~/.config/temp/project-created`. If it does not exist temp will fallback to start `$EDITOR` with the new directory.

## Examples:
Open new Projects in a new VSCode window:
```
#!/bin/sh
code -n .
```

Open a shell:
```
#!/bin/sh
fish -i
```

Use a [justfile](https://github.com/casey/just). This allows different behaviors for the new project depending on the arguments you pass.
```
#!/usr/bin/env -S just --working-directory . --justfile

_init_git_repo:
    git init
    git add .
    git commit -am "Initial Commit"


@_default: _init_git_repo
    fish -i

code: _init_git_repo
    code -n .


clone REPO:
    git clone {{REPO}}
    fish -i
```
So `temp <files>` will use the default recipe, initializing a git repo and starting interactive shell, but running `temp <files> -- clone git@gitlab.com:DrRuhe/temp.git` will clone the repo in the tempdir next to all the files that were copied over.


# Troubleshooting
It might help to see more logs. For that set the `RUST_LOG` environment variable to `debug` or `info` depending on granularity. Example:

```
~$ RUST_LOG=debug temp
```