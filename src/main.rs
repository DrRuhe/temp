use anyhow::{anyhow, bail, Context, Result};
use chrono::Local;
use clap::{Parser, Subcommand};
use directories::ProjectDirs;
use prompts::Prompt;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};
use strum::IntoEnumIterator;
use tokio::fs;
use tracing::{debug, info, instrument};
use walkdir::WalkDir;

use crate::hooks::Hook;

mod hooks;
mod logging;

#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// The files you want in your temporary project
    files: Vec<PathBuf>,
    /// Use symlinks instead of copying files.
    #[clap(short, long)]
    symlink: bool,
    /// Move the files instead of copying them
    #[clap(short, long, conflicts_with("symlink"))]
    r#move: bool,
    /// The name will be added to the folder name.
    #[clap(short, long)]
    name: Option<String>,
    /// The arguments that will be passed to the hook
    #[clap(last = true, value_parser)]
    hook_args: Vec<String>,
    #[clap(subcommand)]
    command: Option<Action>,
}

#[derive(Debug, Subcommand, Clone)]
enum Action {
    /// Remove all the temporary projects by deleting the folder.
    Clean {
        /// Do not promt the deletion again
        #[clap(short, long)]
        force: bool,
    },
    /// Show the files used as hooks and their status.
    Hooks,
}

fn project_directory_path() -> Result<PathBuf> {
    let project_dirs = ProjectDirs::from(".com", "DrRuhe", "temp").ok_or_else(|| {
        anyhow!("No valid home directory path could be retrieved from the operating system")
    })?;
    let projects_dir = project_dirs.data_local_dir().to_path_buf();

    info!("Project directory is: {}", projects_dir.display());
    Ok(projects_dir)
}

#[tokio::main]
async fn main() -> Result<()> {
    logging::init();
    let args = Args::parse();
    debug!(?args, "Loaded Args");

    match args.command {
        None => {
            let project = ProjectConfig::new(args.clone())?;
            debug!(?project, "Created project file mappings");

            create_project_folder(&project, &args).await?;
            Hook::ProjectCreate.run(&project, args.hook_args).await?;
        }
        Some(Action::Hooks) => {
            for hook in Hook::iter() {
                if hook.file_exists().is_ok() {
                    println!(
                        "{} hook file exists at {} (Backup env would be ${})",
                        hook.file_name(),
                        hook.file_path()?.display(),
                        hook.backup_environment_var()
                    );
                } else {
                    println!(
                        "Using ${} since file does not exist at {}",
                        hook.backup_environment_var(),
                        hook.file_path()?.display()
                    );
                }
            }
        }
        Some(Action::Clean { force }) => {
            let projects_dir = project_directory_path()?;

            let confirmation_msg = format!(
                "Do you want to delete all temporary Projects? (This will delete the folder: {} )",
                projects_dir.display()
            );

            if !force {
                let mut prompt =
                    prompts::confirm::ConfirmPrompt::new(confirmation_msg).set_initial(false);

                match prompt.run().await? {
                    Some(true) => {}
                    _ => return Ok(()),
                }
            }

            info!("Deleting folder {}", projects_dir.display());

            fs::remove_dir_all(projects_dir)
                .await
                .context("Deleting Project folder")?;
        }
    };
    Ok(())
}

#[derive(Debug, Serialize, Deserialize)]
struct ProjectConfig {
    temp_dir: PathBuf,
    file_mappings: HashMap<PathBuf, PathBuf>,
}

impl ProjectConfig {
    fn new_project_name(name: Option<String>) -> String {
        let datetime = Local::now();
        let (date, time) = (datetime.date_naive(), datetime.time());
        let timestamp = format!("{}-{}", date.format("%Y-%m-%d"), time.format("%H-%M"));
        match name {
            Some(name) => name,
            None => timestamp,
        }
    }

    fn new(Args { files, name, .. }: Args) -> Result<Self> {
        let temp_dir = project_directory_path()
            .with_context(|| "temp dir of System is not setup as expected")?
            .join(Self::new_project_name(name));

        Ok(Self {
            file_mappings: Self::remap_paths(&temp_dir, files)?,
            temp_dir,
        })
    }

    fn remap_paths(temp_dir: &Path, files: Vec<PathBuf>) -> Result<HashMap<PathBuf, PathBuf>> {
        let result: Result<HashMap<PathBuf, PathBuf>> = files
            .into_iter()
            .flat_map(|mut file| {
                if let Ok(prefix_removed_file) = file.strip_prefix("./") {
                    file = prefix_removed_file.to_path_buf();
                }

                let files: Vec<PathBuf> = if file.is_dir() {
                    WalkDir::new(file)
                        .same_file_system(true)
                        .follow_links(false)
                        .into_iter()
                        .filter_map(|p| p.ok().map(|d| d.into_path()))
                        .filter(|p| p.is_file())
                        .collect()
                } else {
                    std::iter::once(file).collect()
                };

                files.into_iter().map(|file| {
                    let temp_dir_location = temp_dir.join(file.clone());

                    let file_location = file
                        .canonicalize()
                        .with_context(|| format!("Path is invalid: {}", file.display()))?;

                    Ok((temp_dir_location, file_location))
                })
            })
            .collect();

        result
    }
}

#[instrument("Creating project folder Structure")]
async fn create_project_folder(project: &ProjectConfig, args: &Args) -> Result<()> {
    //ensure project directory does not yet exist
    if fs::read_dir(project.temp_dir.clone()).await.is_ok() {
        bail!(
            "Project directory already existed: {}",
            project.temp_dir.display()
        );
    }

    //create project directory
    fs::create_dir_all(&project.temp_dir)
        .await
        .context("Could not create temporary directory")?;

    //copy over all the files
    for (tempdir_file, file) in project.file_mappings.iter() {
        fs::create_dir_all(
            tempdir_file
                .parent()
                .context("file in tempdir did not have parent")?,
        )
        .await
        .context("Could not create subdirectory")?;

        if args.symlink {
            fs::hard_link(file.clone(), tempdir_file.clone())
                .await
                .with_context(|| {
                    format!(
                        "Could not create link: \n link: {}\n dst: {}",
                        tempdir_file.display(),
                        file.display()
                    )
                })?;
        } else {
            fs::copy(file.clone(), tempdir_file.clone())
                .await
                .with_context(|| {
                    format!(
                        "Could not copy file: \n from: {}\n to:   {}",
                        file.display(),
                        tempdir_file.display()
                    )
                })?;
        }

        if args.r#move {
            fs::remove_file(file.clone())
                .await
                .with_context(|| {
                    format!(
                        "Could not remove old file that was copied: \n old file:    {} \n copied file: {}",
                        file.display(),
                        tempdir_file.display()
                    )
                })?;
        }
    }

    Ok(())
}
