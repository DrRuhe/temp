use crate::ProjectConfig;
use anyhow::{anyhow, bail, Context, Result};
use directories::ProjectDirs;
use std::{env, path::PathBuf};
use strum::EnumIter;
use tokio::process::Command;
use tracing::{debug, info, instrument, warn};

fn config_directory_path() -> Result<PathBuf> {
    let project_dirs = ProjectDirs::from(".com", "DrRuhe", "temp").ok_or_else(|| {
        anyhow!("No valid home directory path could be retrieved from the operating system")
    })?;
    let config_dir = project_dirs.config_dir().to_path_buf();

    info!("Config directory is: {}", config_dir.display());
    Ok(config_dir)
}

#[derive(Debug, EnumIter)]
pub(crate) enum Hook {
    ProjectCreate,
}

impl Hook {
    pub(crate) fn file_path(&self) -> Result<PathBuf> {
        let file_name = self.file_name();

        config_directory_path().map(|dirs| dirs.join(file_name))
    }

    pub(crate) fn file_exists(&self) -> Result<()> {
        let file = self.file_path().context("Computing file Path of Hook")?;

        debug!(path = %file.display(),"Looking for {} hook",self.file_name());
        match file.try_exists() {
            Ok(true) => Ok(()),
            Ok(false) => Err(anyhow!(
                "{} hook could not be accessed because of a broken symlink.",
                self.file_name()
            )),
            Err(e) => {
                Err(e).with_context(|| format!("{} hook could not be accessed", self.file_name()))
            }
        }
    }

    pub(crate) fn file_name(&self) -> &'static str {
        match self {
            Hook::ProjectCreate => "project-created",
        }
    }

    pub(crate) fn backup_environment_var(&self) -> &'static str {
        match self {
            Hook::ProjectCreate => "EDITOR",
        }
    }

    fn resolve(&self) -> Result<PathBuf> {
        let hook_name = self.file_name();
        let backup_env = self.backup_environment_var();

        let file_hook = self
            .file_path()
            .map(|file| self.file_exists().map(|_| file));

        let env_hook = env::var(backup_env)
            .with_context(|| format!("Could not access ${backup_env}"))
            .map(|val| {
                PathBuf::try_from(val).with_context(|| {
                    format!("Could not parse ${backup_env} to Path to figure out what to run")
                })
            });

        // some unpacking to remove unnecessary indirections
        let file_hook: Result<PathBuf> = match file_hook {
            Ok(res) => res,
            Err(err) => Err(err),
        };
        let env_hook: Result<PathBuf> = match env_hook {
            Ok(Ok(env_hook)) => Ok(env_hook),
            Ok(Err(err)) => Err(err),
            Err(err) => Err(err),
        };

        let hook = match (file_hook, env_hook) {
            (Ok(file), Ok(_)) => file,
            (Ok(file), Err(e)) => {
                info!(%e);
                file
            }
            (Err(e), Ok(editor)) => {
                warn!(%e);
                editor
            }
            (Err(file_err), Err(env_err)) => bail!(
                "Could not figure out {hook_name} hook.

Using the hook file was not possible because:
{file_err}

Using ${backup_env} was not possible because:
{env_err}
"
            ),
        };

        Ok(hook)
    }

    #[instrument("Running project created hook")]
    pub(crate) async fn run(&self, project: &ProjectConfig, hook_args: Vec<String>) -> Result<()> {
        let hook = self.resolve()?;

        let hook_result = Command::new(&hook)
            .args(hook_args)
            .current_dir(&project.temp_dir)
            .spawn()
            .with_context(|| {
                format!(
                    "Could not start {} hook. \n {} {}",
                    self.file_name(),
                    hook.display(),
                    project.temp_dir.display()
                )
            })?
            .wait()
            .await;

        // Dont care for the result of the hook
        drop(hook_result);
        Ok(())
    }
}
