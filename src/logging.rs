use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, EnvFilter, Layer, Registry};

pub fn init() {
    let logging_layer = {
        let format = tracing_subscriber::fmt::format()
            //.without_time()
            .pretty()
            .with_source_location(true)
            .with_target(false);

        tracing_subscriber::fmt::layer()
            .event_format(format)
            .with_filter(
                EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::from("warn")),
            )
    };

    let subscriber = Registry::default().with(logging_layer);

    tracing::subscriber::set_global_default(subscriber).unwrap();
}
